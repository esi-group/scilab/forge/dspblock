path = get_absolute_file_path('meanblock1.tst');

status = importXcosDiagram(path + '/meanblock1.xcos');
if ~status then pause, end
realmatrix = eval(scs_m.objs(3).graphics.exprs);
scs_m.objs(4).graphics.exprs = ["1";"0"];
xcos_simulate(scs_m, 4);
b = a1('values');
c = mean(realmatrix);
assert_checkequal(b,c);


status = importXcosDiagram(path + '/meanblock1.xcos');
if ~status then pause, end
realmatrix = eval(scs_m.objs(3).graphics.exprs);
scs_m.objs(4).graphics.exprs = ["1";"1"];
xcos_simulate(scs_m, 4);
b = a1('values');
c = mean(realmatrix,1);
assert_checkequal(b,c);


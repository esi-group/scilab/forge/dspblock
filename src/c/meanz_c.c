#include "scicos_block4.h"

void meanz_c(scicos_block *block, int flag)
{
    double *ur = NULL;
    double *ui = NULL;
    double *yr = NULL;
    double *yi = NULL;
    int mu=0, nu=0, i=0, j=0;

    mu = GetInPortRows(block,1);
    nu = GetInPortCols(block,1);
    ur = GetRealInPortPtrs(block,1);
    ui = GetImagInPortPtrs(block,1);
    yr = GetRealOutPortPtrs(block,1);
    yi = GetImagOutPortPtrs(block,1);

    for (i=0; i<mu; i++)
    {
        yr[i] = 0;
        yi[i] = 0;
        for (j=0; j<nu; j++)
        {
            yr[i] += ur[i+j*mu];
            yi[i] += ui[i+j*mu];
        }
        yr[i] /= nu;
        yi[i] /= nu;
    }
}

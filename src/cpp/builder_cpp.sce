// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_cpp()
  src_c_path = get_absolute_file_path("builder_cpp.sce");

  CFLAGS = ilib_include_flag(src_c_path);
  LDFLAGS = "";
  if (getos()<>"Windows") then
    if ~isdir(SCI+"/../../share") then
      // Source version
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos_blocks/includes" ;
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos/includes" ;
    else
      // Release version
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/scicos_blocks";
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/scicos";
    end
  else
    CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos_blocks/includes";
    CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos/includes";

    // Getting symbols
    if findmsvccompiler() <> "unknown" & haveacompiler() then
      LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos.lib""";
      LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos_f.lib""";
    end
  end

  tbx_build_src(..
  ["variance_m", "variance_r", "variance_c", "variancez_m", "variancez_r", "variancez_c",..
   "std_m", "std_r", "std_c", "stdz_m", "stdz_r", "stdz_c",..
   "rms_m", "rms_r", "rms_c", "rmsz_m", "rmsz_r", "rmsz_c",..
   "max_m", "max_r", "max_c", "maxz_m", "maxz_r", "maxz_c",..
   "min_m", "min_r", "min_c", "minz_m", "minz_r", "minz_c"],..
  ["variance_m.cpp", "variance_r.cpp", "variance_c.cpp", "variancez_m.cpp", "variancez_r.cpp", "variancez_c.cpp", "linearalgebra.cpp", "statistics.cpp",..
   "std_m.cpp", "std_r.cpp", "std_c.cpp", "stdz_m.cpp", "stdz_r.cpp", "stdz_c.cpp",..
   "rms_m.cpp", "rms_r.cpp", "rms_c.cpp", "rmsz_m.cpp", "rmsz_r.cpp", "rmsz_c.cpp",..
   "max_m.cpp", "max_r.cpp", "max_c.cpp", "maxz_m.cpp", "maxz_r.cpp", "maxz_c.cpp",..
   "min_m.cpp", "min_r.cpp", "min_c.cpp", "minz_m.cpp", "minz_r.cpp", "minz_c.cpp"],..
  "c",..
  src_c_path,..
  "",..
  LDFLAGS,..
  CFLAGS,..
  "",..
  "",..
  ""); //not sure what goes here
endfunction

builder_cpp();
clear builder_cpp; // remove builder_c on stack

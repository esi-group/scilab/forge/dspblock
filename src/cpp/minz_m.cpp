#include <vector>
#include <iterator>
#include <algorithm>
#include "statistics.h"
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void minz_m(scicos_block *block, int flag)
{
    double *dInputReal = NULL;
    double *dInputImag = NULL;
    double *dValueReal = NULL;
    double *dValueImag = NULL;
    double *nIndex = NULL;
    int nInputRows = 0, nInputCols = 0, nCount = 0;

    nInputRows = GetInPortRows(block, 1);
    nInputCols = GetInPortCols(block, 1);

    dInputReal = GetRealInPortPtrs(block, 1);
    dInputImag = GetImagInPortPtrs(block, 1);
    dValueReal = GetRealOutPortPtrs(block, 1);
    dValueImag = GetImagOutPortPtrs(block, 1);
    nIndex = GetRealOutPortPtrs(block, 2);

    std::vector<double> v(nInputRows*nInputCols);  
    nCount = 0;
    std::vector<double>::iterator viter = v.begin();
    while (nCount < v.size())
    {
        *viter = dInputReal[nCount]*dInputReal[nCount] + \
                 dInputImag[nCount]*dInputImag[nCount];
        nCount++;
        viter++;
    }

    std::vector<double>::iterator dPosMin = std::min_element(v.begin(),v.end());
    nIndex[0] = std::distance(v.begin(),dPosMin);
    dValueReal[0] = dInputReal[(int)(nIndex[0])];
    dValueImag[0] = dInputImag[(int)(nIndex[0])];
}

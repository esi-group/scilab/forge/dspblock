#include <vector>

namespace linearalgebra
{
    extern void transpose(std::vector<double> &v, int nrows, int ncols, std::vector<double> &w);

    extern void printmatrix(std::vector<double> &v, int nrows, int ncols);
}

#include <vector>

namespace statistics
{
    extern double variance(const std::vector<double> &v);
    extern double sumsq(const std::vector<double> &v);
}



extern "C"
{
    #include "scicos_block4.h"
}
#include <vector>
#include "statistics.h"
#include <cmath>

extern "C" void rmsz_m(scicos_block *block, int flag)
{
    double *ur = NULL;
    double *ui = NULL;
    double *y  = NULL;
    int nu = 0, mu = 0;
    mu = GetInPortRows(block,1);
    nu = GetInPortCols(block,1);
    ur = GetRealInPortPtrs(block,1);
    ui = GetImagInPortPtrs(block,1);
    y  = GetRealOutPortPtrs(block,1);
    
    std::vector<double> vreal(ur,ur+mu*nu);
    std::vector<double> vimag(ui,ui+mu*nu);
    y[0] = std::sqrt((statistics::sumsq(vreal) + statistics::sumsq(vimag))/(mu*nu));
}

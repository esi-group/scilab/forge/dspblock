#include <vector>
#include <iterator>
#include <algorithm>
#include "statistics.h"
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void min_r(scicos_block *block, int flag)
{
    double *dInput = NULL;
    double *dValue = NULL;
    double *nIndex = NULL;
    int nInputRows = 0, nInputCols = 0, j = 0;

    nInputRows = GetInPortRows(block, 1);
    nInputCols = GetInPortCols(block, 1);

    dInput = GetRealInPortPtrs(block, 1);
    dValue = GetRealOutPortPtrs(block, 1);
    nIndex = GetRealOutPortPtrs(block, 2);

    std::vector<double> v(dInput, dInput + nInputRows*nInputCols);  //Get matrix elements into a vector
    std::vector<double>::iterator iter = v.begin();
    std::vector<double>::iterator dPosMin;

    for (j=0; j<nInputCols; ++j)
    {
        dPosMin = std::min_element(iter, iter+nInputRows);
        dValue[j] = *dPosMin;
        nIndex[j] = distance(iter,dPosMin);
        iter += nInputRows;
    }
}

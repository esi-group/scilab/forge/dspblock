#include <iostream>
#include <vector>
#include "linearalgebra.h"

int main() {
    std::vector<double> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(6);
    int nrows=3, ncols=2;
    std::cout << "Original matrix" << std::endl;
    linearalgebra::printmatrix(v, nrows, ncols);
    std::cout << std::endl;

    std::cout << "Transpose matrix" << std::endl;
    std::vector<double> w(v.size());
    linearalgebra::transpose(v, nrows, ncols, w);
    linearalgebra::printmatrix(w, ncols, nrows);
    return 0;
}

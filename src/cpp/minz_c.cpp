#include <vector>
#include <iterator>
#include <algorithm>
#include "statistics.h"
#include "linearalgebra.h"
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void minz_c(scicos_block *block, int flag)
{
    double *dInputReal = NULL;
    double *dInputImag = NULL;
    double *dValueReal = NULL;
    double *dValueImag = NULL;
    double *nIndex = NULL;
    int nInputRows = 0, nInputCols = 0, nCount = 0, j=0;

    nInputRows = GetInPortRows(block, 1);
    nInputCols = GetInPortCols(block, 1);

    dInputReal = GetRealInPortPtrs(block, 1);
    dInputImag = GetImagInPortPtrs(block, 1);
    dValueReal = GetRealOutPortPtrs(block, 1);
    dValueImag = GetImagOutPortPtrs(block, 1);
    nIndex = GetRealOutPortPtrs(block, 2);

    std::vector<double> vReal(dInputReal, dInputReal + nInputRows*nInputCols);  //Get matrix elements into a vector
    std::vector<double> vImag(dInputImag, dInputImag + nInputRows*nInputCols);  //Get matrix elements into a vector
    std::vector<double> wReal(vReal.size());
    std::vector<double> wImag(vReal.size());
    linearalgebra::transpose(vReal, nInputRows, nInputCols, wReal);
    linearalgebra::transpose(vImag, nInputRows, nInputCols, wImag);

    std::vector<double> wAbs(nInputRows*nInputCols);
    nCount = 0;
    while (nCount<nInputRows*nInputCols)
    {
        wAbs[nCount] = wReal[nCount]*wReal[nCount] + \
                       wImag[nCount]*wImag[nCount];
        nCount++;
    }
        
    std::vector<double>::iterator wAbsIter = wAbs.begin();
    std::vector<double>::iterator dPosMin;
    for (j=0; j<nInputRows; ++j)
    {
        dPosMin = std::min_element(wAbsIter, wAbsIter+nInputCols);
        nIndex[j] = std::distance(wAbsIter, dPosMin);
        dValueReal[j] = wReal[(int)(nIndex[j]+j*nInputCols)];
        dValueImag[j] = wImag[(int)(nIndex[j]+j*nInputCols)];
        wAbsIter += nInputCols;
    }
}

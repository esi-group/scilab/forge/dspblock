#include <vector>
#include "statistics.h"
#include <cmath>
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void rms_m(scicos_block *block, int flag)
{
    double *u = NULL;
    double *y = NULL;
    int nu = 0, mu = 0, j = 0;

    mu = GetInPortRows(block, 1);
    nu = GetInPortCols(block, 1);

    u = GetRealInPortPtrs(block, 1);
    y = GetRealOutPortPtrs(block, 1);

    std::vector<double> v(u, u+mu*nu);  //Get matrix elements into a vector
    y[0] = std::sqrt((statistics::sumsq(v))/(mu*nu));
}



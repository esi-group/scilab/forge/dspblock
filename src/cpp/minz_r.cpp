#include <vector>
#include <iterator>
#include <algorithm>
#include "statistics.h"
#include "linearalgebra.h"
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void minz_r(scicos_block *block, int flag)
{
    double *dInputReal = NULL;
    double *dInputImag = NULL;
    double *dValueReal = NULL;
    double *dValueImag = NULL;
    double *nIndex = NULL;
    int nInputRows = 0, nInputCols = 0, nCount = 0, i=0;

    nInputRows = GetInPortRows(block, 1);
    nInputCols = GetInPortCols(block, 1);

    dInputReal = GetRealInPortPtrs(block, 1);
    dInputImag = GetImagInPortPtrs(block, 1);
    dValueReal = GetRealOutPortPtrs(block, 1);
    dValueImag = GetImagOutPortPtrs(block, 1);
    nIndex = GetRealOutPortPtrs(block, 2);

    std::vector<double> vAbs(nInputRows*nInputCols);
    nCount = 0;
    std::vector<double>::iterator vAbsIter = vAbs.begin();
    while(nCount < nInputRows*nInputCols)
    {
        *vAbsIter = dInputReal[nCount]*dInputReal[nCount] + \
                 dInputImag[nCount]*dInputImag[nCount];
        nCount++;
        vAbsIter++;
    }

    std::vector<double>::iterator dPosMin;
    vAbsIter = vAbs.begin();
    for (i=0; i<nInputCols; ++i)
    {
        dPosMin = std::min_element(vAbsIter, vAbsIter+nInputRows);
        nIndex[i] = std::distance(vAbsIter, dPosMin);
        dValueReal[i] = dInputReal[(int)(nIndex[i]+i*nInputRows)];
        dValueImag[i] = dInputImag[(int)(nIndex[i]+i*nInputRows)];
        vAbsIter += nInputRows;
    }
}

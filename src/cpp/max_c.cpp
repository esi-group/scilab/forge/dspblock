#include <vector>
#include <iterator>
#include <algorithm>
#include "statistics.h"
#include "linearalgebra.h"
extern "C"
{
#include "scicos_block4.h"
}

extern "C" void max_c(scicos_block *block, int flag)
{
    double *dInput = NULL;
    double *dValue = NULL;
    double *nIndex = NULL;
    int nInputRows = 0, nInputCols = 0, i = 0;

    nInputRows = GetInPortRows(block, 1);
    nInputCols = GetInPortCols(block, 1);

    dInput = GetRealInPortPtrs(block, 1);
    dValue = GetRealOutPortPtrs(block, 1);
    nIndex = GetRealOutPortPtrs(block, 2);

    std::vector<double> v(dInput, dInput + nInputRows*nInputCols);  //Get matrix elements into a vector
    std::vector<double> w(v.size());
    linearalgebra::transpose(v, nInputRows, nInputCols, w);
    std::vector<double>::iterator iter = w.begin();
    std::vector<double>::iterator dPosMax;

    for (i=0; i<nInputRows; ++i)
    {
        dPosMax = std::max_element(iter, iter+nInputCols);
        dValue[i] = *dPosMax;
        nIndex[i] = distance(iter,dPosMax);
        iter += nInputCols;
    }
}

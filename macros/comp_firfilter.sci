// ====================================================================
// Template toolbox_skeleton
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
//
//
function block = comp_firfilter(block, flag)
    if flag == 4 then
        block.x = zeros(size(block.rpar));
    end
    if flag == 2 then //StateUpdate
        for i=block.nx:-1:2
            block.x(i) = block.x(i-1)
        end
        block.x(1) = block.inptr(1)(1);
    end
    if flag == 1 then //OutputUpdate
        block.outptr(1)(1) = block.rpar * block.x;
    end
endfunction
// ====================================================================

function [x,y,typ] = xcos_min(job,arg1,arg2)

    x=[];y=[];typ=[];


    select job

    case 'plot' then
        standard_draw(arg1);

    case 'getinputs' then
        [x,y,typ]=standard_inputs(arg1);

    case 'getoutputs' then
        [x,y,typ]=standard_outputs(arg1);

    case 'getorigin' then
        [x,y]=standard_origin(arg1);

    case 'set' then
        x=arg1;
        model = arg1.model;
        graphics = arg1.graphics;
        exprs = graphics.exprs;

        while %t do
            [ok, datatype, minalong, exprs] = getvalue(['Set MIN block parameters'],..
            ['Data type (1 = real double 2= complex)';..
            'Minimum of (0 = all elements 1 = rows 2 = columns)'],..
            list('vec',1,'vec',1),..
            exprs);

            if ~ok
                break;
            end

            if (datatype == 1)
                if (minalong == 0) then
                    junction_name = 'min_m';
                    out = [1 1;..
                           1 1];
                elseif (minalong == 1) then
                    junction_name = 'min_r';
                    out = [1 -2;..
                           1 -2];
                elseif (minalong == 2) then
                    junction_name = 'min_c';
                    out = [-1 1;..
                           -1 1];
                else
                    message(['Min along must be 0, 1 or 2']);
                    ok = %f;
                end
                it = 1;
                ot = [1; 1];
            elseif (datatype == 2)
                if (minalong == 0) then
                    junction_name = 'minz_m';
                    out = [1 1;..
                           1 1];
                elseif (minalong == 1) then
                    junction_name = 'minz_r';
                    out = [1 -2;..
                           1 -2];
                elseif (minalong == 2) then
                    junction_name = 'minz_c';
                    out = [-1 1;..
                           -1 1];
                else
                    message(['Min along must be 0, 1 or 2']);
                    ok = %f;
                end
                it = 2;
                ot = [2; 1];
            else
                message('Data type is not supported');
                ok = %f;
            end

            in = [model.in model.in2];

            if ok
                [model, graphics, ok] = set_io(model, graphics, list(in,it), list(out,ot), [], []);
            end

            if ok then
                model.sim       = list(junction_name, 4);
                arg1.model      = model;
                graphics.exprs  = exprs;
                arg1.graphics   = graphics;
                x               = arg1;
                break;
            end
        end

    case 'define' then
        model=scicos_model();
        junction_name = 'min_m';
        model.sim   = list(junction_name, 4);
        model.in    = -1;
        model.in2   = -2;
        model.intyp = 1;
        model.out   = [1; 1];
        model.out2  = [1; 1];
        model.outtyp= [1; 1];

        model.blocktype='c';
        model.dep_ut=[%t %f]; //depends on input, not on time

        exprs = [sci2exp(1);sci2exp(0)];
        gr_i = [''];

        x=standard_define([2 2],model,exprs,gr_i);
    end
endfunction


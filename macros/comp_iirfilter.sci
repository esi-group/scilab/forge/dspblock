// ====================================================================
// Template toolbox_skeleton
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
function block = comp_iirfilter(block, flag)
    if flag == 4 then
        block.x = zeros(size(block.rpar));
    end
    if flag == 2 then //StateUpdate
        numsize = block.rpar(1);
        //state vector consists for x's followed by y's
        for i=numsize:-1:2
            block.x(i) = block.x(i-1);
        end
        block.x(1) = block.inptr(1)(1);
        for i=block.nx:-1:numsize+2
            block.x(i) = block.x(i-1);
        end
        block.x(numsize+1) = block.outptr(1)(1);
    end
    if flag == 1 then //OutputUpdate
        block.outptr(1)(1) = block.rpar(2:$) * block.x;
    end
endfunction
// ====================================================================

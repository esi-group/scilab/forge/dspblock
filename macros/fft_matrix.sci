// ====================================================================
// Template toolbox_skeleton
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
//
//
function block = fft_matrix(block, flag)
    if flag == 1 then
        noofrows = block.insz(1);
        noofcols = block.insz(2);
        if noofrows == 1 then
            //row vector input
            v = [];
            for colindex = 1:noofcols
                v = [v; block.inptr(1)(colindex)];
            end
            w = fft(v);
            for colindex = 1:noofcols
                block.outptr(1)(colindex) = w(colindex);
            end
        else
            //matrix input
            for colindex = 1:noofcols
                v = [];
                for rowindex = 1:noofrows
                    linearindex = (colindex-1)*noofrows + rowindex;
                    v = [v; block.inptr(1)(linearindex)];
                end
                w = fft(v);
                for rowindex = 1:noofrows
                    linearindex = (colindex-1)*noofrows + rowindex;
                    block.outptr(1)(linearindex) = w(rowindex);
                end
            end
        end
    end
endfunction
// ====================================================================

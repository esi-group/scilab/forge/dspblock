function [x,y,typ] = xcos_fft(job,arg1,arg2)

    x=[];y=[];typ=[];


    select job

    case 'plot' then
        standard_draw(arg1);

    case 'getinputs' then
        [x,y,typ]=standard_inputs(arg1);

    case 'getoutputs' then
        [x,y,typ]=standard_outputs(arg1);

    case 'getorigin' then
        [x,y]=standard_origin(arg1);

    case 'set' then
        x=arg1;
        model = arg1.model;
        graphics = arg1.graphics;
        exprs = graphics.exprs;

        ok = %t;
        while %t do
            if ~ok
                break;
            end

            junction_name = 'fft_matrix';
            in = [model.in model.in2];
            out = [model.in model.in2];
            it = 2;
            ot = 2;

            if ok
                [model, graphics, ok] = set_io(model, graphics, list(in,it), list(out,ot), [], []);
            end

            if ok then
                model.sim       = list(junction_name, 5);
                arg1.model      = model;
                graphics.exprs  = exprs;
                arg1.graphics   = graphics;
                x               = arg1;
                break;
            end
        end

    case 'define' then
        model=scicos_model();
        junction_name = 'fft_matrix';
        model.sim   = list(junction_name, 5);
        model.in    = -1;
        model.in2   = -2;
        model.intyp = 1;
        model.out   = 1;
        model.out2  = 1;
        model.outtyp= 1;

        model.blocktype='c';
        model.dep_ut=[%t %f]; //depends on input, not on time

        exprs = [sci2exp(1);sci2exp(0)];
        gr_i = [''];

        x=standard_define([2 2],model,exprs,gr_i);
    end
endfunction

